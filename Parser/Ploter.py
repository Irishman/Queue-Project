import matplotlib.pyplot as plt
import numpy as np


def show_plot(x, y, title, x_label, y_label):
    plt.figure(1)
    plt.subplot(111)

    plt.axis([-1, np.max(x), -1, np.max(y)])

    plt.axhline(y=0, linewidth=1, color='black')
    plt.axvline(x=0, linewidth=1, color='black')

    plt.plot(x, y, 'darkgreen', label=r'U', marker='o')

    plt.title(title)
    plt.xlabel(x_label, color='blue')
    plt.ylabel(y_label, color='blue')

    plt.show()
import Ploter
import datetime

file = open('../ConsumerData/Message.txt', 'r');
lines = file.readlines()

dates = []

rows_number = len(lines)

for line in lines:
    temp = line.split(" ")
    times = temp[2].split("\n")[0]
    dates.append(datetime.datetime.strptime(times, "%H:%M:%S"))

min_time = min(dates)
max_time = max(dates)

seconds = (max_time - min_time).seconds

rps = rows_number/seconds

print(rps)

x = [1, 2, 4, 5, 6, 7, 8, 9, 10, 13, 13, 14]
y = [1.32, 1.9, 2.1, 2.323, 0.327, 0.326, 0.3211, 0.3212, 1.3213, 2.324, 2.3210, 2.529]

'''
Ploter.show_plot(x, y, "Sovsem Test", "Test", "Test2")
'''
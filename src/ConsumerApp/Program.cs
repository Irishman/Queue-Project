﻿namespace ConsumerApp
{
    using System;
    using System.Threading;

    using Queue.Kafka;

    using Microsoft.Extensions.CommandLineUtils;

    public static class Program
    {
        private const string Topic = "Test";

        private static readonly int _batch = 3;
        private static string _broker = "127.0.0.1";

        private static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                cts.Cancel();
                eventArgs.Cancel = true;
            };

            var app = new CommandLineApplication
            {
                Name = "ConsumerApp"
            };

            app.HelpOption("-?|-h|--help");

            var brokerOption = app.Option("--BROKER <value>", "Set broker endpoint", CommandOptionType.SingleValue);

            app.OnExecute(() =>
            {
                if (brokerOption.HasValue())
                {
                    _broker = brokerOption.Value();
                }

                using (var consumer = new QueueConsumer(new[] { Topic }, _broker, _batch))
                {
                    consumer.RunAsync(cts.Token).GetAwaiter().GetResult();
                    return 0;
                }
            });
            app.Execute(args);
        }
    }
}

﻿namespace QueueAPIGateway.Controllers
{
    using System;

    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;

    using Queue.Objects;
    
    [Route("api/[controller]")]
    public class NonEfficientController : Controller
    {
        private static readonly Worker Worker = new Worker();

        // POST api/nonefficient
        [HttpPost]
        public IActionResult Post([FromBody] string message)
        {
            var myOrder = new Order()
            {
                Date = DateTime.Now,
                Id = Guid.NewGuid(),
                Message = message
            };
            try
            {
                var json = JsonConvert.SerializeObject(myOrder);
                Worker.LongWork(json).GetAwaiter().GetResult();
                return Ok(myOrder);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
﻿namespace QueueAPIGateway.Controllers
{
    using System;
    
    using Microsoft.AspNetCore.Mvc;

    using Queue.Objects;
    using Queue.Kafka;
    
    [Route("api/[controller]")]
    public class KafkaController : Controller
    {
        private readonly IQueueProducer _producer;
        
        public KafkaController(IQueueProducer producer)
        {
            _producer = producer;
        }
        
        // POST api/kafka
        [HttpPost]
        public IActionResult Post([FromBody] string message)
        {
            var myOrder = new Order()
            {
                Date = DateTime.Now,
                Id = Guid.NewGuid(),
                Message = message
            };
            try
            {
                _producer.RunAsync(myOrder).GetAwaiter().GetResult();
                return Accepted(myOrder);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}

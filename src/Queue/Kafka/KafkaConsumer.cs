﻿namespace Queue.Kafka
{
    using System;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Reactive.Linq;
    
    using Confluent.Kafka;
    using Confluent.Kafka.Serialization;
    
    public class KafkaConsumer
    {
        private readonly Consumer<string, string> _consumer;
        private readonly IEnumerable<string> _topics;
        
        public KafkaConsumer(IEnumerable<string> topics, string brokerEndpoints, string groupId)
        {
            _topics = topics;
            var config = new Dictionary<string, object>
                {
                    { "bootstrap.servers", brokerEndpoints },
                    { "api.version.request", true },
                    { "group.id", string.IsNullOrEmpty(groupId) ? Guid.NewGuid().ToString() : groupId },
                    { "socket.blocking.max.ms", 1 },
                    { "enable.auto.commit", false },
                    { "fetch.wait.max.ms", 5 },
                    { "fetch.error.backoff.ms", 5 },
                    { "fetch.message.max.bytes", 10240 },
                    { "queued.min.messages", 1000 },
                    {
                        "default.topic.config",
                        new Dictionary<string, object>
                            {
                                { "auto.offset.reset", "beginning" }
                            }
                    }
                };
            _consumer = new Consumer<string, string>(config, new StringDeserializer(Encoding.UTF8), new StringDeserializer(Encoding.UTF8));
        }

        public IObservable<Message<string, string>> Consume(CancellationToken cancellationToken)
        {
            var observable = Observable.FromEventPattern<Message<string, string>>(
                                 x =>
                                 {
                                     _consumer.OnMessage += x;
                                     _consumer.Subscribe(_topics);
                                 },
                                 x =>
                                 {
                                     _consumer.Unsubscribe();
                                     _consumer.OnMessage -= x;
                                 })
                             .Select(x => x.EventArgs);

            Task.Factory.StartNew(
                    () =>
                    {
                        while (!cancellationToken.IsCancellationRequested)
                        {
                            _consumer.Poll(TimeSpan.FromMilliseconds(100));
                        }
                    },
                    cancellationToken,
                    TaskCreationOptions.LongRunning,
                    TaskScheduler.Default)
                .ConfigureAwait(false);

            return observable;
        }

        public async Task CommitAsync(Message<string, string> message)
        {
            await _consumer.CommitAsync(message);
        }

        public void Dispose()
        {
            _consumer?.Unsubscribe();
            _consumer?.Dispose();
        }
    }
}
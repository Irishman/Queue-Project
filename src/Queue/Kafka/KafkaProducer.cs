﻿namespace Queue.Kafka
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    
    using Confluent.Kafka;
    using Confluent.Kafka.Serialization;
    
    public class KafkaProducer : IDisposable
    {
        private readonly Producer<string, string> _producer;
        
        public KafkaProducer(string brokerEndpoints)
        {
            var config = new Dictionary<string, object>
            {
                { "bootstrap.servers", brokerEndpoints },
                { "api.version.request", true },
                { "socket.blocking.max.ms", 1 },
                { "queue.buffering.max.ms", 5 },
                { "queue.buffering.max.kbytes", 1 },
                {
                    "default.topic.config",
                    new Dictionary<string, object>
                    {
                        { "message.timeout.ms", 30 },
                        { "request.required.acks", -1 }
                    }
                }
            };
            _producer = new Producer<string, string>(config, new StringSerializer(Encoding.UTF8), new StringSerializer(Encoding.UTF8));
        }

        public async Task<Message<string, string>> ProduceAsync(string key, string value, string topic, int partition = -1)
        {
            Message<string, string> result;
            
            if (partition < 0)
            {
                result = await _producer.ProduceAsync(topic, key, value);
            }
            else
            {
                result = await _producer.ProduceAsync(topic, key, value, partition);
            }

            if (result.Error.HasError)
            {
                throw new KafkaException(result.Error);
            }

            return result;
        }

        public void Dispose()
        {
            _producer.Dispose();
        }
    }
}

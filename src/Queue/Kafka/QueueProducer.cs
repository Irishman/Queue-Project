﻿namespace Queue.Kafka
{
    using System;
    using System.Threading.Tasks;
    
    using Newtonsoft.Json;
    using Confluent.Kafka;
    
    using Objects;
    
    public class QueueProducer : IDisposable, IQueueProducer
    {
        private readonly string _topic;
        private readonly KafkaProducer _kafkaProducer;
        
        public QueueProducer(string brokerEndpoint, string topic)
        {
            _topic = topic;
            _kafkaProducer = new KafkaProducer(brokerEndpoint);
        }

        public async Task<Message<string, string>> RunAsync(Order newOrder)
        {
            var value = JsonConvert.SerializeObject(newOrder);
            var key = newOrder.Id.ToString();

            Message<string, string> message;
            try
            {
                message = await _kafkaProducer.ProduceAsync(key, value, _topic);             
                Console.WriteLine($"Message '{message.Value}' produced to '{message.Topic}/{message.Partition} @{message.Offset}'");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Kafka producer return error '{e.Message}'");
                throw;
            }
            return message;
        }

        public void Dispose()
        {
            _kafkaProducer.Dispose();
        }
    }
}

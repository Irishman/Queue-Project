﻿namespace Queue.Kafka
{
    using System.Threading.Tasks;
    
    using Confluent.Kafka;
    
    using Objects;
    
    public interface IQueueProducer
    {
        Task<Message<string, string>> RunAsync(Order newOrder);
    }
}
﻿namespace Queue.Kafka
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Reactive.Linq;
    using System.Threading;

    using Objects;
    
    public class QueueConsumer : IDisposable
    {
        private const string GroupId = "Queue-Consumer";

        private readonly KafkaConsumer _kafkaConsumer;
        private readonly Worker _worker;
        private readonly int _batchSize;

        private IDisposable _registration;

        public QueueConsumer(IEnumerable<string> topics, string brokerEndpoints, int batchSize)
        {
            _batchSize = batchSize;
            _worker = new Worker();
            _kafkaConsumer = new KafkaConsumer(topics, brokerEndpoints, GroupId);
        }

        public async Task RunAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Consumer start RunAsync.");
            var observable = _kafkaConsumer.Consume(cancellationToken);

            var subscription = observable.Buffer(_batchSize)
                      .Subscribe(
                          messages =>
                          {
                              var tasks = messages.Select(async (message) =>
                              {
                                  Console.WriteLine(
                                      $"{message.Topic}/{message.Partition} @{message.Offset}: '{message.Value}'");
                                  await _worker.LongWork(message.Value);
                              });
                              Task.WhenAll(tasks).GetAwaiter().GetResult();
                              _kafkaConsumer.CommitAsync(messages[messages.Count - 1]).GetAwaiter().GetResult();
                          });

            var taskCompletionSource = new TaskCompletionSource<object>();
            _registration = cancellationToken.Register(
                () =>
                {
                    subscription.Dispose();
                    taskCompletionSource.SetResult(null);
                });

            await taskCompletionSource.Task;
        }

        public void Dispose()
        {
            _kafkaConsumer.Dispose();
        }
    }
}
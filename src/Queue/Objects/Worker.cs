﻿namespace Queue.Objects
{
    using System.Threading.Tasks;
    using System.IO;
    using System;

    using Newtonsoft.Json.Linq;
    
    public class Worker
    {
        public async Task LongWork(string messageValue)
        {
            var message = JObject.Parse(messageValue);

            string json = $"TransactionDate: {DateTime.Now}\n";

            File.AppendAllText("ConsumerData/Message.txt", json);
            // Long work.
            await Task.Delay(3 * 1000);
        }
    }
}
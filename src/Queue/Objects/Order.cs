﻿namespace Queue.Objects
{
    using System;

    public class Order
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
    }
}
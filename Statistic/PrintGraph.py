import pandas as pd
import Ploter


PATH_TO_FILES = "CSV_Files/"

data = pd.read_csv(PATH_TO_FILES + "requests_1528100940.99.csv")

rps = data[['Name', 'Requests/s']]
rps1 = data['Requests/s']

# print(rps)
print(rps1)

x = [1, 2, 4, 5, 6, 7, 8, 9, 10, 13, 13, 14]
y = [1.32, 1.9, 2.1, 2.323, 0.327, 0.326, 0.3211, 0.3212, 1.3213, 2.324, 2.3210, 2.529]

Ploter.show_plot(x, y, "Sovsem Test", "Test", "Test2")
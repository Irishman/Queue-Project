from locust import HttpLocust, TaskSet, task
import json


class UserBehavior(TaskSet):

    @task(1)
    def get_kafka(self):
        result_string = 'Test message'*5
        data = {'message': result_string}
        self.client.headers['Content-Type'] = "application/json"
        self.client.post("/api/kafka", data=json.dumps(data))


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
